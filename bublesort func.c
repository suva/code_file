#include <stdio.h>
int arr[100];
void bubble_sort(int n)
{
    int i, j, temp;

    for(i =  n- 1; i >= 0; i--)
    {
        for( j = 0; j < i; j++)
        {
            if(arr[j] > arr[j + 1])
            {
                temp = arr[j];
                arr[j]= arr[ j+ 1];
                arr[j + 1] = temp;
            }
        }
    }
}
int main()
{
    int n;
    while(scanf("%d", &n))
    {
        int i;
        for(i = 0; i < n; i++)
        {
            scanf("%d",&arr[i]);
        }
        printf("Before Sorting\n");
        for(i = 0; i < n; i++)
        {
            printf("%d ", arr[i]);
        }
        printf("\n\n");

        bubble_sort(n);

         printf("After Sorting\n");
        for( i = 0; i < n; i++)
        {
            printf("%d ", arr[i]);
        }
        printf("\n\n");


    }
}
