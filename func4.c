#include <stdio.h>
#include <math.h>
int prime_check(int n)
{
    int i, sq = sqrt(n);

    for(i = 2; i <= sq; i++)
    {
        if(n % i == 0)
        {
            return 0;
        }
    }

    return 1;
}
int revers (int n)
{
    int res = 0;

    while(n > 0)
    {
        res = res * 10 + n % 10;
        n = n / 10;
    }
    return res;
}
int main()
{
    int i;
    for( i = 2; i <= 20; i++)
    {
        if(prime_check(i) == 1)
        {
            printf("%d ", revers(i));

        }
    }
}
