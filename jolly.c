#include <stdio.h>


int main()
{
    int A[100], B[100];

    int n, i, j;
    ///input
    scanf("%d", &n);
    for(i = 0; i < n; i++)
    {
        scanf("%d", &A[i]);
    }

    /// process

    /// step 1
    int x;
    for(i = 0; i < n - 1; i++)
    {
        x = A[i] - A[i + 1];
        //printf("%d\n",x);
        if(x < 0)
        {
            x = x * (-1);
        }
        B[i] = x;
    }
    n = n - 1;
    /*
     for(i = 0; i < n; i++)
     {
         printf("%d ", B[i]);
     }
     printf("\n");
     step 1 done
    */
    /// step 2
    int temp;
    for(i = n - 1; i > 0; i--)
    {
        for(j = 0; j < i; j++)
        {

             if(B[j] > B[j + 1])
            {
                temp = B[j];
                B[j] = B[j + 1];
                B[j + 1] = temp;
            }
        }
    }

    ///
    /*
    for(i = 0; i < n; i++)
    {
        printf("%d ", B[i]);
    }
    step 3 done
    */

    /// Step  4
    int cnt = 0;
    for(i = 0; i < n; i++)
    {
        if(B[i] == i+1)
        {
            cnt++;
        }
    }
     /// printf("%d\n", cnt);
    if(cnt == n)
        printf("Jolly\n");                  ///where n=n-1
    else
        printf("Not jolly\n");
}
