#include<iostream>
using namespace std;
    class Base{
    public:
    virtual void show(){
    cout<<"Base class";
    }
    };
        class Derived:public Base
        {
        public:
        void show(){
        cout<<"Derived class";
        }
        };

int main()
{
 Base*b= new Base;

 Derived*d= new Derived;
b=&d;

b->show();
d->show();
return 0;
}
